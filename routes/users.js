var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var db = require('./../modules/database/MyMongodb');
var app = express();
var d = db.getInstance()

app.use(function (req, res, next) {
  console.log(req.body)
  next();
})

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/editInfo', function(req, res, next) {
  console.log(req.body)
  d.changeUserInfo({'name': {$in:[req.body.name]}},
      {'name': req.body.oname,'email': req.body.email, 'password': req.body.password, 'uuid':req.body.uuid},
      {'name': req.body.name,'email': req.body.email, 'password': req.body.password, 'uuid':req.body.uuid},
      'user', res)
});

router.post('/editPassword', function(req, res, next) {
  console.log(req.body)
  d.updateDataBase(
      {'name': req.body.name,'email': req.body.email, 'password': req.body.opassword, 'uuid': req.body.uuid},
      {'name': req.body.name,'email': req.body.email, 'password': req.body.password, 'uuid': req.body.uuid},
      'user',
      res)
})

module.exports = router;
