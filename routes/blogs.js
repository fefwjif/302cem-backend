var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var db = require('./../modules/database/MyMongodb');
var app = express();
var d = db.getInstance()

app.use(function (req, res, next) {
  console.log(req.body)
  next();
})

/* GET users listing. */
router.get('/', function(req, res, next) {
  d.readDataBase({},'blog', res)
});

router.post('/add', function(req, res, next) {
  d.addDataBase({'name': req.body.name,'title': req.body.title, 'content': req.body.content,
    'photo': req.body.photo, 'date': req.body.date, 'uuid':req.body.uuid},'blog', res)
});

router.post('/remove', function(req, res, next) {
  d.delDataBase({'name': req.body.name,'title': req.body.title, 'content': req.body.content,
    'photo': req.body.photo, 'date': req.body.date,'uuid':req.body.uuid},'blog', res)
});

router.post('/edit', function(req, res, next) {
  console.log(req.body)
  d.updateDataBase({'name': req.body.oname,
    'title': req.body.otitle, 'content': req.body.ocontent, 'photo': req.body.ophoto,
    'date': req.body.odate,'uuid':req.body.uuid},
      {'name': req.body.name,'title': req.body.title, 'content': req.body.content, 'photo': req.body.photo,
        'date': req.body.date,'uuid':req.body.uuid},'blog', res)
});

router.post('/myBlogs', function(req, res, next) {
  console.log(req.body)
  d.readDataBase({'uuid': {$in:[req.body.uuid]}},'blog', res)
});

module.exports = router;