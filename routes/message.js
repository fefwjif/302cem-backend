var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var db = require('./../modules/database/MyMongodb');
var app = express();
var d = db.getInstance()

app.use(function (req, res, next) {
    console.log(req.body)
    next();
})

router.post('/', function(req, res, next) {
    console.log(req.body)
    d.readDataBase({'uuid':{$in: [req.body.uuid]}},'message',res)
});

router.post('/send', function(req, res, next) {
    console.log(req.body)
    d.sendMessage({'name': {$in:[req.body.recipient]}},req.body.sender,
        req.body.message,'user', 'message', res)
});

module.exports = router;
