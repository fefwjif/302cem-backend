var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(function (req, res, next) {
    console.log(req.body)
    next();
})

class ErrorOrSuccessful{

    static getInstance(){

        if(!ErrorOrSuccessful.instance){
            ErrorOrSuccessful.instance= new ErrorOrSuccessful();
        }
        return  ErrorOrSuccessful.instance;
    }

    responseSuccessful (successful){
        //var j = JSON.stringify(successful)
        return successful
    }

    responseError (error){
        return error
    }
}

module.exports = ErrorOrSuccessful;


