var MongoClient = require('mongodb').MongoClient;
var Databaseconfig = require('./../../config')

var DBURL = Databaseconfig.DBURL
var dbName = Databaseconfig.DBName

var express = require('express');
var app = express();

var errorOrSuccessful = require('./../result/ErrorOrSuccessful')
var responseResult = errorOrSuccessful.getInstance()

var r;

app.use(function (req, res, next) {
    console.log(req.body)
    next();
})

class MyMongodb{

    static getInstance(){

        if(!MyMongodb.instance){
            MyMongodb.instance= new MyMongodb();
        }
        return  MyMongodb.instance;
    }

    updateDataBase (filter, update, table, res) {
        MongoClient.connect(DBURL,{ useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
            if (err) throw err;

            var db = client.db(dbName)

            db.collection(table).updateOne(filter,{ $set: update}, function (error, result) {
                if (error) {
                    r = responseResult.responseError(error.message)
                    res.send(r)
                }
                r = responseResult.responseSuccessful({'ok':'successful'})
                res.send(r)
            })
            client.close()
        })
    }

    readDataBase (find, table, res) {
        MongoClient.connect(DBURL,{ useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
            if (err) throw err ;

            var list= [];
            var db = client.db(dbName)
            var result = db.collection(table).find(find);

            result.each(function (error, doc) {
                if (error) {
                    console.log(responseResult.responseError(error))
                    r = responseResult.responseError(error)
                    res.send(r)
                } else {
                    if (doc != null) {
                        console.log(doc)
                        list.push(doc);
                    } else {
                        console.log(doc)
                        r = responseResult.responseSuccessful(list)
                        res.send(r)
                        client.close()
                    }

                }
            })
        })
    }

    login (find, table, res) {
        MongoClient.connect(DBURL,{ useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
            if (err) throw err ;

            var db = client.db(dbName)
            db.collection(table).findOne(find, function (err, document) {
                if (err) {
                    r = responseResult.responseError({'ok': err.message})
                    res.send(r)
                    return
                }else{
                    if (document == null) {
                        r = responseResult.responseError({'name':'---','password':'---','uuid':'---','email':'---'})
                        res.send(r)
                    } else {
                        r = responseResult.responseSuccessful(document)
                        res.send(r)
                    }
                }
            })
        })
    }

    changeUserInfo (filter, old, changed ,table, res) {
        MongoClient.connect(DBURL,{ useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
            if (err) throw err ;

            var db = client.db(dbName)
            db.collection(table).findOne(filter, function (err, document) {
                if (err) {
                    r = responseResult.responseError({'ok': err.message})
                    res.send(r)
                    return
                }else{
                    if (document == null) {
                        MyMongodb.getInstance().updateDataBase(old, changed, table, res)
                    } else {
                        r = responseResult.responseError({'ok': 'This name has be used'})
                        res.send(r)
                    }
                }
            })
        })
    }

    sendMessage (filter,s,m, findTable, addTable, res) {
        MongoClient.connect(DBURL,{ useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
            if (err) throw err ;

            var db = client.db(dbName)
            db.collection(findTable).findOne(filter, function (err, document) {
                if (err) {
                    r = responseResult.responseError({'ok': err.message})
                    res.send(r)
                    return
                }else{
                    if (document == null) {
                        r = responseResult.responseError({'ok': 'No this user'})
                        res.send(r)
                    } else {
                        MyMongodb.getInstance().addDataBase({'recipient':document.name, 'uuid':document.uuid, 'message':m, 'sender':s}
                            ,addTable,res)
                    }
                }
            })
        })
    }

    addDataBase (json, table, res) {
        MongoClient.connect(DBURL,{ useNewUrlParser: true, useUnifiedTopology: true}, function (err, client) {
            if (err) throw err;

            var db = client.db(dbName)

            db.collection(table).insertOne(json, function (error, result) {
                if (error) {
                    r = responseResult.responseError(error.message)
                    res.send({'ok':error.message})
                }
                r = responseResult.responseSuccessful('yes')
                res.send({'ok':'successful'})
                client.close()
            })
        })
    }

    delDataBase (del,  table, res) {
        MongoClient.connect(DBURL, { useNewUrlParser: true, useUnifiedTopology: true}, function (err, client) {
            if (err) throw err;

            var db = client.db(dbName)
            db.collection(table).deleteOne(del, function (error, obj) {

                if (error) {
                    r = responseResult.responseError(error.message)
                    res.send(r)
                }
                r = responseResult.responseSuccessful({'ok':'successful'})
                res.send(r)
                client.close()
            })
        })
    }

     createUser(json, fliter, table, res) {
        MongoClient.connect(DBURL,{  useNewUrlParser: true, useUnifiedTopology: true}, function (err, client) {
            if (err) throw err;

            var db = client.db(dbName)
            db.collection(table).findOne(fliter, function (err, document) {
                if (err) {
                    r = responseResult.responseError({'ok': error.message})
                    res.send(r)
                    return
                }else{
                    if (document == null) {
                        MyMongodb.instance.addDataBase(json, table, res)
                    } else {
                        r = responseResult.responseError({'ok':'this account had be created'})
                        res.send(r)
                    }
                }
            })
        })
    }
}
module.exports = MyMongodb;